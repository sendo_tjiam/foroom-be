@extends('layouts.app')

@section('content')
<div class="container_regist no_padding">
    <div class="background_image"></div>
    <div class="col-sm-12 col-md-7 background_image_blur_register"></div>

    <section class="regist_container row d-flex text-white">
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="left_regis d-flex flex-column col-sm-12 col-md-6">
                <h1 class="mb-0">Register</h1>
                <label class="label_input" for="name">{{ __('username') }}</label>
                <input id="name" class="short_input @error('name') is-invalid @enderror" name="name" type="text"
                    placeholder="Enter your Username" value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <label class="label_input" for="email">{{ __('email') }}</label>
                <input id="email" class="short_input @error('email') is-invalid @enderror" type="email" name="email"
                    placeholder="Enter your Email Address" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <label class="label_input" for="password">{{ __('password') }}</label>
                <input id="password" class="short_input @error('password') is-invalid @enderror" type="password"
                    name="password" placeholder="Enter your Password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <label class="label_input" for="confirm-password">{{ __('confirm password') }}</label>
                <input id="confirm-password" class="short_input @error('confirm-password') is-invalid @enderror" type="password" name="confirm-password"
                    placeholder="Confirm your Password" onchange="confirmPassword()" required autocomplete="new-password">
                    <span id="confimPass-message" style="color: red"></span>
                @error('confirm-password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                    
            </div>
            <div class="d-flex flex-column  right_regis col-sm-12 col-md-6">
                <label class="label_input mt-5">Select your gaming platform</label>
                <select class="short_input form-control @error('subject') is-invalid @enderror" name="subject" id="subject" required>
                    <option value="pc computer">PC/Computer</option>
                    <option value="console">Console</option>
                    <option value="board game">Board Game</option>
                    <option value="mobile">Mobile</option>
                    <option value="arcade">Arcade Machine</option>
                </select>
                @error('subject')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <label class="label_input mt-0" for="bio">Bio</label>
                <textarea class="long_input @error('bio') is-invalid @enderror" name="bio" id="bio" placeholder="Describe yourself to be displayed in your profile"
                    id="" cols="30" rows="10" required></textarea>
                @error('bio')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <p class="mt-1 mb-0"><input type="checkbox" name="agreement_check" label="agree"><label
                        class="small_text ml-1 mb-0" for="agree">I have agreed to the terms & conditions</label> </p>
            </div>
            <button class="btn btn-regis" type="submit">{{ __('Register') }}</button>
        </form>
    </section>
</div>
@endsection
