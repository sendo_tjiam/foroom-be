<footer class="bg-dark row footer py-5">
    <div class="footer_left d-flex flex-column col-md-4 mt-3 justify-content-center align-items-center">
        <img class=" align-self-center footer-logo" src="{{ asset('img/ggLogo.png') }}" alt="gg_logo">
        <img class=" align-self-center footer-logo-txt" src="{{ asset('img/Game Globe.png') }}" alt="Game Globe">
    </div>
    <div class="col-md-4 footer_mid pt-3">
        <p class="text-white d-flex flex-column">
            <a class="font-weight-bold footer-head" href="forum_main.html">Forums</a>
            <a class="footer-sub-head" href="#">PC Games</a>
            <a class="footer-sub-head" href="#">Console</a> 
            <a class="footer-sub-head" href="#">Board Games</a>
            <a class="footer-sub-head" href="#">Mobile Games</a>
            <a class="footer-sub-head" href="#">Arcade Machines</a>
            <a class="font-weight-bold footer-head" href="#">About Game Globe</a>
            <a class="font-weight-bold footer-head" href="#">Sign In to Game Globe</a>
            <a class="font-weight-bold footer-head" href="#">Back to Top</a>
        </p>   
    </div>
    <div class=" col-md-4 pt-3 footer_right">
        <h3 class="font-weight-bold footer-head">Contact us in our social media!</h3>
        <div class="d-flex flex-column">
            <a class="footer-sub-head footer-social mt-3" href="#"><img class="footer-icon" src="../img/Icon simple-twitter.png" alt="">Game Global</a>
            <a class="footer-sub-head footer-social mt-2" href="#"><img class="footer-icon" src="../img/Icon awesome-facebook-square.png" alt="">GG forum</a>
            <a class="footer-sub-head footer-social mt-2" href="#"><img class="footer-icon" src="../img/Icon awesome-instagram.png" alt="">GGbestforum</a>
        </div>
    </div>
    <div class="col-md-12 text-center text-white">© Game Globe 2020 All Rights Reserved</div>
</footer>