<section class="row hero">
    <div class="col-md-12 d-flex align-items-center hero_section"></div>
    <p class="d-flex flex-column hero_txt text-white">
        <span class="hero_heading">Welcome to Game Globe Forum!</span>
        <span class="hero_subheading">Share your thoughts on the topics Bellow</span>
        <span class="hero_subheading">or</span>
        <span class="hero_subheading">Share some new interesting topics to talk about</span>
    </p>
</section>