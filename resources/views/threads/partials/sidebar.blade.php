<div class="sidebar">
    <a href="{{ route('threads.index' )}}" class="sidebar-list d-block">
        <button class="btn active_nav align-center">All Threads</button>
    </a>

    <div class="sidebar-subjects">
        @foreach ($subjects as $subject)
            <a href="{{ route('subjects.show', $subject )}}" class="sidebar-subjects-list d-block">
                {{ $subject->name }} 
                <span class="float-right">{{ $subject->threads_count }}</span>
            </a>
        @endforeach
    </div>

    <div class="sidebar-action">
        <p>Action</p>
        <a href="{{ route('threads.create') }}" class="sidebar-action-list d-block">Create New Thread</a>
        <a href="{{ route('threads.popular') }}" class="sidebar-action-list d-block">Popular Threads</a>
        <a href="{{ route('threads.mine') }}" class="sidebar-action-list d-block">My Threads</a>
        <a href="{{ route('threads.unanswered') }}" class="sidebar-action-list d-block">Unanswered Threads</a>
    </div>
</div>

<div class="sidebar">
</div>