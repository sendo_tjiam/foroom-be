@extends('layouts.app')

@section('content')
@include('threads.partials.landing')
<div class="container my-5">

    <div class="col-md-12 row header-font my-3">
        Game Discussion
    </div>
    <div class="row">
        <div class="col-md-4">
            @include('threads.partials.sidebar')
        </div>
        <div class="col-md-8">
            @include('alert')
            @foreach ($threads as $thread)
                <div class="card thread mb-3">
                    <div class="card-body thread-body">
                        <a href="{{ route('threads.show', $thread->slug) }}" class="threads-content">
                            <div class="threads-body">
                                <div class="threads-title">{{ $thread->title }}</div>
                                <div class="thread-published">
                                    {{ $thread->created_at->diffForHumans() }}
                                    &middot;
                                    {{ $thread->user->name }}
                                    &middot;
                                    {{ $thread->subject->name }}
                                    &middot;
                                    {{ $thread->replies_count }} Replies
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
            {!! $threads->links() !!}
        </div>
    </div>
</div>
@include('threads.partials.footer')
@endsection
