<?php

use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
            'name' => 'PC/Computer',
            'slug' => 'pc-computer'
        ]);

        DB::table('subjects')->insert([
            'name' => 'Console',
            'slug' => 'console'
        ]);

        DB::table('subjects')->insert([
            'name' => 'Board Game',
            'slug' => 'board-game'
        ]);

        DB::table('subjects')->insert([
            'name' => 'Mobile',
            'slug' => 'mobile'
        ]);

        DB::table('subjects')->insert([
            'name' => 'Arcade Machine',
            'slug' => 'arcade-machine'
        ]);
    }
}
