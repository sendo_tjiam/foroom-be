<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Reply extends Model
{
    protected $fillable = ['description', 'user_id', 'hash'];

    public function getRouteKeyName() {
        return 'hash';
    }
    
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function thread() {
        return $this->belongsTo(Thread::class);
    }
}
