<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subject;
use App\Thread;
use App\User;

class ThreadController extends Controller
{
    public function __contsruct() {
        return $this->middleware('auth', ['except' => 'index']);
    }

    public function index() {
        $threads = Thread::latest()->paginate(6);
        $subjects = Subject::all();
        return view('threads.index', compact('threads', 'subjects'));
    }

    public function mine() {
        $threads = auth()->user()->threads()->latest()->paginate(6);
        $subjects = Subject::all();
        return view('threads.index', compact('threads', 'subjects'));
    }

    public function popular() {
        $threads = Thread::has('replies')->orderByDesc('replies_count')->paginate(6);
        $subjects = Subject::all();
        return view('threads.index', compact('threads', 'subjects'));
    }

    public function unanswered() {
        $threads = Thread::doesntHave('replies')->paginate(6);
        $subjects = Subject::all();
        return view('threads.index', compact('threads', 'subjects'));
    }

    public function create() {
        $subjects = Subject::all();
        return view('threads.create', compact('subjects'));
    }

    public function store() {

        $this->validate(request(), [
            'title' => 'required',
            'description' => 'required',
            'subject' => 'required'
        ]);

        $slug = str_slug(request('title'));
        
        // one to many implementation
        $thread = auth()->user()->threads()->create([
            'title' => request('title'),
            'slug' => $slug,
            'description' => request('description'),
            'subject_id' => request('subject')
        ]);

        return redirect()->route('threads.show', $thread->slug)->with('success', 'New Thread Added');
    }

    public function show($slug) {
        $thread = Thread::whereSlug($slug)->first();
        $subjects = Subject::all();
        $replies = $thread->replies;
        return view('threads.show', compact('thread', 'replies', 'subjects'));
    }

    public function edit(Thread $thread) {
        $this->authorize('update', $thread);
        $subjects = Subject::all();
        return view('threads.edit', compact('thread', 'subjects'));
    }

    public function update(Thread $thread) {
        $this->authorize('update', $thread);
        $this->validate(request(), [
            'title' => 'required',
            'description' => 'required',
            'subject' => 'required'
        ]);

        $slug = str_slug(request('title'));
        $thread->update([
            'title' => request('title'),
            'slug' => $slug,
            'description' => request('description'),
            'subject_id' => request('subject')
        ]);

        return redirect()->route('threads.show', $thread->slug)->with('success', 'Your Thread Updated');
    }

    public function destroy(Thread $thread) {
        $thread->delete();
        return redirect()->route('threads.mine')->with('success', 'Thread deleted');
    }
}
