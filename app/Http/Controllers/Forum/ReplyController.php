<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Thread;
use App\Reply;
use App\Subject;

class ReplyController extends Controller
{
    public function __contsruct() {
        return $this->middleware('auth');
    }
    
    public function store(Thread $thread) {
        $this->validate(request(), ['description' => 'required']);
        $thread->replies()->create([
            'user_id' => auth()->id(),
            'description' => request('description'),
            'hash' => str_random(32)
        ]);
        return back();
    }

    public function edit(Reply $reply) {    
        $this->authorize('update', $reply);
        $subjects = Subject::all();
        return view('replies.edit', compact('reply', 'subjects'));
    }

    public function update(Reply $reply) {
        $this->authorize('update', $reply);
        $this->validate(request(), ['description' => 'required']);

        $reply->update([
            'description' => request('description')
        ]);

        return back()->with('success', 'Successfully Edit Reply!');
    }
}
