<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasManyThread;
use App\Thread;

class Subject extends Model
{
    use HasManyThread;

    protected $withCount = ['threads'];

    public function getRouteKeyName() {
        return 'slug';
    }
}
