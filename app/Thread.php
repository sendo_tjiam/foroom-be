<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Thread;
use App\Subject;

class Thread extends Model
{
    protected $fillable = ['title', 'slug', 'description', 'subject_id'];

    protected $withCount = ['replies'];

    // relation user-thread
    public function user() {
        return $this->belongsTo(User::class);
    }

    // relation thread-subject
    public function subject() {
        return $this->belongsTo(Subject::class);
    }

    public function replies() {
        return $this->hasMany(Reply::class);
    }
}
